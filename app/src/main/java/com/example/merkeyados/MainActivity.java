package com.example.merkeyados;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.merkeyados.models.Product;

import java.io.Serializable;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    private EditText etName;
    private EditText etDescription;
    private EditText etCantidad;
    private EditText etCost;
    private Button btnSave;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadViews();
        setupOnClicks();
    }

    private void setupOnClicks() {

            View.OnClickListener onClick;
            onClick = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    save(v);
                }
            };
            btnSave.setOnClickListener(onClick);
        }

    private void openConfirmActivity(Product product) {
        Intent intent = new Intent(this, confirmActivity.class);
        intent.putExtra("product", product);
        startActivity(intent);
    }

    private void loadViews() {

        etName = findViewById(R.id.et_name);
        etDescription = findViewById(R.id.et_description);
        etCantidad = findViewById(R.id.et_cantidad);
        etCost = findViewById(R.id.et_cost);
        btnSave = findViewById(R.id.btn_save);

    }

    public void save (View view){
        String name = etName.getText().toString().trim();
        String description = etDescription.getText().toString().trim();
        String cantidad = etCantidad.getText().toString().trim();
        String cost = etCost.getText().toString().trim();

        boolean isSuccess = true;

        if (name.isEmpty()) {
            etName.setError("Requerido");
            isSuccess = false;
        }

        if (description.isEmpty()) {
            etDescription.setError("Requerido");
            isSuccess = false;
        }

        if (cantidad.isEmpty()) {
            etCantidad.setError("Requerido");
            isSuccess = false;
        }

        if (cost.isEmpty()) {
            etCost.setError("Requerido");
            isSuccess = false;
        }

        if (isSuccess) {
            String id = UUID.randomUUID().toString();
            Product product = new Product(id, name, description, cantidad, cost);
            openConfirmActivity(product);

        }



    }









}
