package com.example.merkeyados.models;

import java.io.Serializable;

public class Product implements Serializable {

    private String id;
    private String name;
    private String description;
    private String cantidad;
    private String cost;

    public Product() {
    }

    public Product(String id, String name, String description, String cantidad, String cost) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.cantidad = cantidad;
        this.cost = cost;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }
}
